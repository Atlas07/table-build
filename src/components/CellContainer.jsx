import React from "react"
import PropTypes from "prop-types"
import { Table } from "semantic-ui-react"

import InnerContainer from "../components/InnerContainer"

const CellContainer = ({ data }) => (
  <Table.Cell textAlign="center">
    {data.value}
    {data.children && <InnerContainer data={data.children} />}
  </Table.Cell>
)

CellContainer.propTypes = {
  data: PropTypes.shape({
    value: PropTypes.string.isRequired
  }).isRequired
}

export default CellContainer
