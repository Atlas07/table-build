import React from "react"
import PropTypes from "prop-types"
import { Table } from "semantic-ui-react"

import CellContainer from "./CellContainer"

const InnerContainer = ({ data }) => (
  <Table celled>
    <Table.Body>
      <Table.Row>
        {data.map(child => (
          <CellContainer key={Math.random()} data={child} />
        ))}
      </Table.Row>
    </Table.Body>
  </Table>
)

InnerContainer.propTypes = {
  data: PropTypes.shape({
    value: PropTypes.string.isRequired
  }).isRequired
}

export default InnerContainer
