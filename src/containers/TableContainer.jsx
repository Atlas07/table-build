import React from "react"
import PropTypes from "prop-types"
import { Table } from "semantic-ui-react"

import CellContainer from "../components/CellContainer"

class TableContainer extends React.Component {
  state = {}

  render() {
    const { data } = this.props

    return (
      <Table celled>
        <Table.Body>
          <Table.Row>
            {!!data &&
              data.map(title => (
                <CellContainer key={Math.random()} data={title} />
              ))}
          </Table.Row>
          <Table.Row>
            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>

            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>

            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>
            <Table.Cell>Test</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    )
  }
}

TableContainer.propTypes = {
  data: PropTypes.arrayOf({
    value: PropTypes.string.isRequired
  }).isRequired
}

export default TableContainer
