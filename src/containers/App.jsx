import React, { Component } from "react"
import axios from "axios"
import { Header, Loader } from "semantic-ui-react"
import TableContainer from "./TableContainer"

class App extends Component {
  state = {
    loading: true,
    data: {}
  }

  componentDidMount() {
    axios
      .get("http://demo4452328.mockable.io/table/1")
      .then(res => res.data)
      .then(data => {
        this.setState({ loading: false, data })
      })
      .catch(err => {
        console.log(err)
        this.setState({ loading: false })
      })
  }

  render() {
    const { loading, data } = this.state

    if (loading) {
      return <Loader active>Loading</Loader>
    }

    return (
      <div>
        <Header as="h1" textAlign="center">
          Table Build
        </Header>
        <TableContainer data={data.title} content={data.content} />
      </div>
    )
  }
}

export default App
